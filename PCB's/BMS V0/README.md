# Técnico Solar Boat's BMS V0 PCB

This PCB is a test PCB for the BMS that is being developed by TSB with the LTC6811 chip.

This version of the PCB is mostly functional except for MOSFETS that control balancing, they are positioned incorrectly on the PCB due to a schematic error. In addition the LED placed on the LTC6811-2 (LTC) GPIO1 will not work as the LTC pins are open drain and this was not taken into account at the time of the schematic. (LTC should cut the led GND not the other way around). 



Copyright (C) 2019  Técnico Solar Boat

This repository and its contents  is free software: you can redistribute 
it and/or modify it under the terms of the GNU General Public License 
as published by the Free Software Foundation, either version 3 of the 
License, or (at your option) any later version.

The content of this repository is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

You can contact Técnico Solar Boat by email at: tecnico.solarboat@gmail.com
or via our facebook page at https://fb.com/tecnico.solarboat