/*
    Copyright (C) 2019  Técnico Solar Boat

    This program is free software: you can redistribute 
    it and/or modify it under the terms of the GNU General Public License 
    as published by the Free Software Foundation, either version 3 of the 
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    You can contact Técnico Solar Boat by email at: tecnico.solarboat@gmail.com
    or via our facebook page at https://fb.com/tecnico.solarboat
*/

#ifndef BMS_H
#define BMS_H

#include <Arduino.h>  // typedefs use types defined in this header file.
#include <stdint.h>
#include "BMS_config.h"
#include "LT_SPI.h"
#include "LTC6811.h"
#include "LTC681x.h"
#include <SPI.h>
#include "NTC.h"


extern uint16_t minimumCellVoltage;



typedef struct{
    int16_t LowRange;
    int16_t FullRange;
} BatCurrent;

void BMS_init( cell_asic ic[] );
void BMS_Wake_Write_Config( cell_asic ic[] );
void BMS_selfCheck( cell_asic ic[], uint16_t *error  );
uint8_t BMS_readCells_GPIO_1_2( cell_asic ic[], uint16_t *error );
bool BMS_needs_balance( cell_asic ic[], uint16_t *min );
void BMS_balance( cell_asic ic[], uint16_t min );
void BMS_discharge_OV( cell_asic ic[] );
void BMS_check_balance( cell_asic ic[], uint16_t setpoint);
bool BMS_any_balancing(cell_asic ic[] );
uint8_t BMS_read_aux( cell_asic ic[], uint16_t *error);
uint8_t BMS_OV_UV( cell_asic ic[], uint8_t *warn, uint16_t *error);
bool BMS_is_cell_charged( cell_asic ic[]);


void bms_print_cells( cell_asic ic[] );
void bms_print_aux( cell_asic ic[] );
void bms_check_error(int error);
void bms_print_stat( cell_asic ic[] );

#endif  // BMS_H