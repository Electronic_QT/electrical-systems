/*
    Copyright (C) 2019  Técnico Solar Boat

    This program is free software: you can redistribute 
    it and/or modify it under the terms of the GNU General Public License 
    as published by the Free Software Foundation, either version 3 of the 
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    You can contact Técnico Solar Boat by email at: tecnico.solarboat@gmail.com
    or via our facebook page at https://fb.com/tecnico.solarboat
*/


#include "BMS.h"

BatCurrent batCurrent;
int16_t SolarCurrent;



void BMS_init( cell_asic ic[] ){
	spi_enable(SPI_CLOCK_DIV16); // This will set the Linduino to have a 1MHz Clock
	LTC681x_init_cfg(TOTAL_IC, ic);
	LTC6811_reset_crc_count(TOTAL_IC,ic);
	LTC6811_init_reg_limits(TOTAL_IC,ic);
	wakeup_sleep(TOTAL_IC);
	LTC6811_clrcell();
	wakeup_sleep(TOTAL_IC);
	LTC6811_clraux();
	wakeup_sleep(TOTAL_IC);
	LTC6811_clrstat();
}

void BMS_Wake_Write_Config( cell_asic ic[] ){
	wakeup_sleep(TOTAL_IC);
	LTC6811_wrcfg(TOTAL_IC, ic);
}

void BMS_selfCheck( cell_asic ic[], uint16_t *error ){
	/*
	Bit0: errors detected in Digital Filter and CELL Memory;
	Bit1: errors detected in Digital Filter and AUX Memory;
	Bit2: errors detected in Digital Filter and STAT Memory;
	Bit3: Mux Test;
	Bit4: ADC Overlap self test;
	Bit5: AUX Measurement;
	Bit6: STAT Measurement;
	Bit7: Open wire error;
	Bit8: Thermal shutdown status (1 means True 0 means False);
	Bit11: A PEC error was detected in the received data;
	Bit15 to Bit12: Cell that is open, this is a 4 bit decimal number to indicate the cell connection that is open.
	*/
	// The configuration is not writen in this function because we want the ADC options to have the defualt value 7 kHz
	int8_t aux = 0;
	// ADC's Self Test
	wakeup_sleep(TOTAL_IC);
	aux = LTC6811_run_cell_adc_st(CELL,TOTAL_IC,ic);
	if ( aux != 0 ) {
		*error = *error | 0x0001;
		aux = 0;
	}
	
	wakeup_sleep(TOTAL_IC);
	aux = LTC6811_run_cell_adc_st(AUX,TOTAL_IC, ic);
	if ( aux != 0 ) {
		*error = *error | 0x0002;
		aux = 0;
	}

	wakeup_sleep(TOTAL_IC);
	aux = LTC6811_run_cell_adc_st(STAT,TOTAL_IC, ic);
	if ( aux != 0 ) {
		*error = *error | 0x0004;
		aux = 0;
	}

	// Mux Self Test
	wakeup_sleep(TOTAL_IC);
	LTC6811_diagn();
	delay(5);
	aux = LTC6811_rdstat(0,TOTAL_IC,ic); // Set to read back all aux registers
	if ( aux != 0 ) { // PEC Error detected
		*error = *error | 0x0800;
		aux = 0;
	}
	for (int iccount = 0; iccount<TOTAL_IC; iccount++)
	{
		if (ic[iccount].stat.mux_fail[0] != 0) aux++;
	}
	if ( aux != 0 ) {
		*error = *error | 0x0008;
		aux = 0;
	}

	// Run ADC Overlap self test
	wakeup_sleep(TOTAL_IC);
	aux = (int8_t)LTC6811_run_adc_overlap(TOTAL_IC,ic);
	if ( aux != 0 ) {
		*error = *error | 0x0010;
		aux = 0;
	}

	// ADC Digital Redundancy self test
	wakeup_sleep(TOTAL_IC);
	aux = LTC6811_run_adc_redundancy_st(ADC_CONVERSION_MODE,AUX,TOTAL_IC, ic);
	if ( aux != 0 ) {
		*error = *error | 0x0020;
		aux = 0;
	}

	wakeup_sleep(TOTAL_IC);
	aux = LTC6811_run_adc_redundancy_st(ADC_CONVERSION_MODE,STAT,TOTAL_IC, ic);
	if ( aux != 0 ) {
		*error = *error | 0x0040;
		aux = 0;
	}

	// Open wire test
	LTC6811_run_openwire(TOTAL_IC, ic);
	for (int current_ic = 0 ; current_ic < TOTAL_IC; current_ic++)
	{
		if (ic[current_ic].system_open_wire != 0)
		{
			*error = *error | 0x0080;
			for (int cell=0; cell<ic[0].ic_reg.cell_channels+1; cell++)
			{
				if ((ic[current_ic].system_open_wire &(1<<cell))>0)
				{
					*error = *error | (cell << 12 );
				}
			}
		}
	}

	// Thermal Shutdown
	wakeup_sleep(TOTAL_IC);
	LTC6811_rdstat(2,TOTAL_IC,ic);
	delay(200);
	aux = LTC6811_rdstat(2,TOTAL_IC,ic); // Set to read back all aux registers
	if ( aux != 0 ) { // PEC Error detected
		*error = *error | 0x0800;
		aux = 0;
	}
	if ( ic[0].stat.thsd[0] != 0 ){
		*error = *error | 0x0100;
	}

	// Serial.print("BMS_selfCheck: ");
	// Serial.println(*error);
	// Send error to serial
	// Trigger buzzer if error != 0 and block code 
}


uint8_t BMS_readCells_GPIO_1_2( cell_asic ic[], uint16_t *error){
	int8_t PEC_error = 0;
	uint32_t conv_time = 0;

	// Send command to measure the Cell and GPIO 1 and 2 voltages
	BMS_Wake_Write_Config( ic );
	LTC6811_adcvax(ADC_CONVERSION_MODE, ADC_DCP);
	conv_time = LTC6811_pollAdc();
	//Serial.print(F("cell conversion completed in:"));
	//Serial.print(((float)conv_time/1000), 1);
	//Serial.println(F("mS"));
	//Serial.println();

	wakeup_sleep(TOTAL_IC);
	PEC_error = LTC6811_rdcv(0, TOTAL_IC,ic); // Set to read back all cell voltage registers
	bms_check_error(PEC_error);
	//bms_print_cells( ic );

	wakeup_sleep(TOTAL_IC);
	PEC_error = LTC6811_rdaux(0,TOTAL_IC,ic); // Set to read back all aux registers
	bms_check_error(PEC_error);
	//bms_print_aux( ic );

	if ( PEC_error != 0) {
		*error = *error | 0x0800;
		// Send Stautus Message 0x01
		// Buzzer
		return 1;
	}
	return 0;
}

// Checks if there is any cell neding to be balanced
bool BMS_needs_balance( cell_asic ic[], uint16_t *min ){
	uint16_t max = 0;
	uint16_t inbalance = 0;
	*min = 42000;
	for(int i = 0; i < ic[0].ic_reg.cell_channels; i++){
		if ( ic[0].cells.c_codes[i] > max ){
			max = ic[0].cells.c_codes[i];
		}
		if ( ic[0].cells.c_codes[i] < *min ){
			*min = ic[0].cells.c_codes[i];
		}
	}
	inbalance = max - *min;
	Serial.println((String) "Imbalance = " + inbalance);
	Serial.println((String) "Minimum Cell Voltage is: " + *min);
	if ( inbalance > inbalance_thershold ) {
		return 1;
	}
	return 0;
}

// Activates balance on cells that need it
void BMS_balance( cell_asic ic[], uint16_t min ){
	for(int i = 0; i < ic[0].ic_reg.cell_channels; i++){
		if (ic[0].cells.c_codes[i] > ( min + inbalance_thershold) ) {
			Serial.println((String)"Cell " + (i+1) + " is set to balance!");
			LTC6811_set_discharge(i+1,TOTAL_IC,ic); 
			// +1 because LTC6811_set_discharge is done to recive cell number from 1 to 12
		}	
	}
	BMS_Wake_Write_Config( ic );
}

// Active discharge on overvoltaged cells
void BMS_discharge_OV( cell_asic ic[] ){
	for(int i = 0; i < ic[0].ic_reg.cell_channels; i++){
		if ( ic[0].cells.c_codes[i] > OV_THRESHOLD ){
			LTC6811_set_discharge(i+1,TOTAL_IC,ic);
			// +1 because LTC6811_clear_discharge is done to recive cell number from 1 to 12
		}
	}
}

// Deactivates balance in cells that are alredy within the balancing threshold
void BMS_check_balance( cell_asic ic[], uint16_t setpoint){
	bool haveChanged = false;
	for(int i = 0; i < ic[0].ic_reg.cell_channels; i++){
		if ( (ic[0].cells.c_codes[i] < ( setpoint + inbalance_thershold)) && (ic[0].cells.c_codes[i] <= OV_THRESHOLD) ) {
			haveChanged = true;
			LTC6811_clear_discharge(i+1,TOTAL_IC,ic); 
			// +1 because LTC6811_clear_discharge is done to recive cell number from 1 to 12
		}	
	}
	if (haveChanged) {
		BMS_Wake_Write_Config(ic);
	}
}

// Checks if there is still any cell ballancing
bool BMS_any_balancing(cell_asic ic[] ){
	bool bal[12];
	
	for (int current_ic = 0 ; current_ic < TOTAL_IC; current_ic++)
	{
		uint16_t current_mask = 0x01;
		for(int i = 0; i < 8; i++)
		{
			bal[i] = (ic[current_ic].config.rx_data[4] & current_mask) >> i;
			current_mask = current_mask << 1;
		}
		current_mask = 0x01;
		for(int i = 8; i < 12; i++)
		{
			bal[i] = (ic[current_ic].config.rx_data[5] & current_mask) >> (i-8);
			current_mask = current_mask << 1;
		}
		for(int i = 0; i < 12; i++)
		{
			if (bal[i]) {
				return true;
			}
				
		}
		return false;
	}
	return false;
}

// Reads SOC, LTC temperature, VregA, VregD and GPIO 3 to 5
uint8_t BMS_read_aux( cell_asic ic[], uint16_t *error){
	int8_t PEC_error = 0;

	BMS_Wake_Write_Config( ic );
	for(uint8_t CH_TO_CONVERT = 3; CH_TO_CONVERT < 6; CH_TO_CONVERT++){ // This will convert GPIO 3 to 5
		LTC6811_adax(MD_7KHZ_3KHZ, CH_TO_CONVERT);
		LTC6811_pollAdc();
	}

	wakeup_sleep(TOTAL_IC);
	PEC_error = LTC6811_rdaux(0,TOTAL_IC,ic);
	bms_check_error(PEC_error);
	bms_print_aux( ic );

	wakeup_sleep(TOTAL_IC);
	LTC6811_adstat(MD_7KHZ_3KHZ, STAT_CH_TO_CONVERT);
	LTC6811_pollAdc();

	wakeup_sleep(TOTAL_IC);
	PEC_error = LTC6811_rdstat(0,TOTAL_IC,ic);
	bms_check_error(PEC_error);
	bms_print_stat( ic );
	
	if ( PEC_error != 0) {
		*error = *error | 0x0800;
		// Send Stautus Message 0x01
		// Buzzer
		return 1;
	}
	return 0;
}

// Checks OV and UV flags
uint8_t BMS_OV_UV( cell_asic ic[], uint8_t *warn, uint16_t *error){
	int8_t PEC_error = 0;
	uint8_t aux = NONE;
	bool OV[12];
	bool UV[12];

	BMS_Wake_Write_Config( ic );
	PEC_error = LTC6811_rdstat(2,TOTAL_IC,ic); // Set to read back all aux registers
	if ( PEC_error != 0) {
		*error = *error | 0x0800;
		// Send Stautus Message 0x01
		// Buzzer
	}
	
	for (int current_ic =0 ; current_ic < TOTAL_IC; current_ic++)
	{
		for(int i = 0, k = 0; i < 4; i++, k++)
		{
			UV[i] = ( ic[current_ic].stat.flags[0] >> k ) & 0x01;
			k++;
			OV[i] = ( ic[current_ic].stat.flags[0] >> k ) & 0x01;	
		}
		for(int i = 4, k = 0; i < 8; i++, k++)
		{
			UV[i] = ( ic[current_ic].stat.flags[1] >> k ) & 0x01;
			k++;
			OV[i] = ( ic[current_ic].stat.flags[1] >> k ) & 0x01;		
		}
		for(int i = 8, k = 0; i < 12; i++, k++)
		{
			UV[i] = ( ic[current_ic].stat.flags[2] >> k ) & 0x01;
			k++;
			OV[i] = ( ic[current_ic].stat.flags[2] >> k ) & 0x01;
		}
		
		for(int i = 0; i < 12; i++)
		{
			if (OV[i]!= 0) {
				*warn = *warn | 0x04;
				aux = OVER_VOLTAGE;
				break;
			}
		}
		for(int i = 0; i < 12; i++)
		{
			if (UV[i]!= 0) {
				*warn = *warn | 0x08;
				aux = UNDER_VOLTAGE;
				break;
			}
		}
	}
	return aux;
} 

// Checks if any cell have the reached the OV_THRESHOLD
bool BMS_is_cell_charged( cell_asic ic[]){
	for (int i=0; i< ic[0].ic_reg.cell_channels; i++){
		if (ic[0].cells.c_codes[i] >= OV_THRESHOLD) {
			return true;
		}
		return false;
	}
	return false;
}



void bms_print_cells( cell_asic ic[] )
{
	for (int current_ic = 0 ; current_ic < TOTAL_IC; current_ic++)
	{	
		Serial.print(" IC ");
		Serial.print(current_ic+1,DEC);
		Serial.print(", ");
		for (int i=0; i< ic[0].ic_reg.cell_channels; i++)
		{

			Serial.print(" C");
			Serial.print(i+1,DEC);
			Serial.print(":");
			Serial.print(ic[current_ic].cells.c_codes[i]*0.0001,4);
			Serial.print(",");
		}
		Serial.println();
		
	}
	Serial.println();
}

void bms_print_aux( cell_asic ic[] )
{
	for (int current_ic =0 ; current_ic < TOTAL_IC; current_ic++)
	{
		Serial.print(" IC ");
		Serial.print(current_ic+1,DEC);
		for (int i=0; i < 5; i++) // Ler ate 5 para ler o resto dos GPIOS
		{
			Serial.print(F(" GPIO-"));
			Serial.print(i+1,DEC);
			Serial.print(":");
			Serial.print(ic[current_ic].aux.a_codes[i]*0.0001,4);
			Serial.print(",");
		}
		// Serial.print(F(" Vref2"));
		// Serial.print(":");
		// Serial.print(ic[current_ic].aux.a_codes[5]*0.0001,4);
		// Serial.println();
	}
	Serial.println();
}

void bms_print_stat( cell_asic ic[] )
{

	for (int current_ic =0 ; current_ic < TOTAL_IC; current_ic++)
	{
		Serial.print(F(" IC "));
		Serial.print(current_ic+1,DEC);
		Serial.print(F(" SOC:"));
		Serial.print(ic[current_ic].stat.stat_codes[0]*0.0001*20,4);
		Serial.print(F(","));
		Serial.print(F(" Itemp:"));   
		Serial.print(ic[current_ic].stat.stat_codes[1]*0.0001,4);
		Serial.print(F(","));
		Serial.print(F(" LTC Temp:"));   
		Serial.print(ic[current_ic].stat.stat_codes[1]*0.01333333-273,4);
		Serial.print(F(","));
		Serial.print(F(" VregA:"));
		Serial.print(ic[current_ic].stat.stat_codes[2]*0.0001,4);
		Serial.print(F(","));
		Serial.print(F(" VregD:"));
		Serial.print(ic[current_ic].stat.stat_codes[3]*0.0001,4);
		Serial.println();
	}

	Serial.println();
}

//Function to check error flag and print PEC error message
void bms_check_error(int error)
{
	if (error == -1)
	{
		Serial.println(F("A PEC error was detected in the received data"));
	}
}