//! @todo Review this document.
/*!
LT_SPI: Routines to communicate with ATmega328P's hardware SPI port.

@verbatim

LT_SPI implements the low level master SPI bus routines using
the hardware SPI port.

SPI Frequency = (CPU Clock frequency)/(16+2(TWBR)*Prescaler)
SPCR = SPI Control Register (SPIE SPE DORD MSTR CPOL CPHA SPR1 SPR0)
SPSR = SPI Status Register (SPIF WCOL - - - - - SPI2X)

Data Modes:
CPOL  CPHA  Leading Edge    Trailing Edge
0      0    sample rising   setup falling
0      1    setup rising    sample falling
1      0    sample falling  setup rising
1      1    sample rising   setup rising

CPU Frequency = 16MHz on Arduino Uno
SCK Frequency
SPI2X  SPR1  SPR0  Frequency  Uno_Frequency
  0      0     0     fosc/4     4 MHz
  0      0     1     fosc/16    1 MHz
  0      1     0     fosc/64    250 kHz
  0      1     1     fosc/128   125 kHz
  0      0     0     fosc/2     8 MHz
  0      0     1     fosc/8     2 MHz
  0      1     0     fosc/32    500 kHz

@endverbatim


Copyright 2018(c) Analog Devices, Inc.

All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
 - Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
 - Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in
   the documentation and/or other materials provided with the
   distribution.
 - Neither the name of Analog Devices, Inc. nor the names of its
   contributors may be used to endorse or promote products derived
   from this software without specific prior written permission.
 - The use of this software may or may not infringe the patent rights
   of one or more patent holders.  This license does not release you
   from the requirement that you obtain separate licenses from these
   patent holders to use this software.
 - Use of the software either in source or binary form, must be run
   on or directly connected to an Analog Devices Inc. component.
   
THIS SOFTWARE IS PROVIDED BY ANALOG DEVICES "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, NON-INFRINGEMENT,
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
IN NO EVENT SHALL ANALOG DEVICES BE LIABLE FOR ANY DIRECT, INDIRECT,
INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
LIMITED TO, INTELLECTUAL PROPERTY RIGHTS, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

//! @ingroup Linduino
//! @{
//! @defgroup LT_SPI LT_SPI: Routines to communicate with ATmega328P's hardware SPI port.
//! @}

/*! @file
    @ingroup LT_SPI
    Library for LT_SPI: Routines to communicate with ATmega328P's hardware SPI port.
*/


#include <Arduino.h>
#include <stdint.h>
#include <SPI.h>
#include "BMS_config.h"
#include "LT_SPI.h"

// SPI Settings for the comunication with the LTC6811.
// SETS SPI mode to 3 (CPHA and CPOL = 1)
// Most Significant Byte First
// SPI speed
SPISettings LTC6811_SPI_config(SPISettings(500000, MSBFIRST, SPI_MODE3));


// Setup the processor for hardware SPI communication.
// Must be called before using the other SPI routines.
// Alternatively, call quikeval_SPI_connect(), which automatically
// calls this function.
void spi_enable(uint8_t spi_clock_divider) // Configures SCK frequency. Use constant defined in header file.
{  
	pinMode(LTC_CS, OUTPUT);
	output_high(LTC_CS); //! 1) Pull Chip Select High
	output_high(13);
	SPI.begin();
	SPI.setClockDivider(spi_clock_divider);
	// Empety transaction to make the bus comply with the SPI_MODE3 specifications
	SPI.beginTransaction(LTC6811_SPI_config);
	spi_write(0xFF);
	SPI.endTransaction();
}

// Disable the SPI hardware port
void spi_disable()
{
	SPI.end();
}

// Write a data byte using the SPI hardware
void spi_write(int8_t  data)  // Byte to be written to SPI port
{
	SPI.transfer(data);
}

// Read and write a data byte using the SPI hardware
// Returns the data byte read
int8_t spi_read(int8_t  data) //!The data byte to be written
{
	return SPI.transfer(data);
}

/*
Writes an array of bytes out of the SPI port
*/
void spi_write_array(uint8_t len, // Option: Number of bytes to be written on the SPI port
										 uint8_t data[] //Array of bytes to be written on the SPI port
										)
{
	for (uint8_t i = 0; i < len; i++)
	{
		SPI.beginTransaction(LTC6811_SPI_config);
		SPI.transfer((int8_t)data[i]);
		SPI.endTransaction();
	}
	//Serial.println();
}

/*
 Writes and read a set number of bytes using the SPI port.

*/

void spi_write_read(uint8_t tx_Data[],//array of data to be written on SPI port
										uint8_t tx_len, //length of the tx data arry
										uint8_t *rx_data,//Input: array that will store the data read by the SPI port
										uint8_t rx_len //Option: number of bytes to be read from the SPI port
									 )
{
	SPI.beginTransaction(LTC6811_SPI_config);
	for (uint8_t i = 0; i < tx_len; i++)
	{
		
		SPI.transfer(tx_Data[i]);
	}

	for (uint8_t i = 0; i < rx_len; i++)
	{

		rx_data[i] = (uint8_t)SPI.transfer(0xFF);
	}
	SPI.endTransaction();

}


uint8_t spi_read_byte(uint8_t tx_dat)
{
	uint8_t data;
	SPI.beginTransaction(LTC6811_SPI_config);
	data = (uint8_t)SPI.transfer(0xFF);
	SPI.endTransaction();
	return(data);
}
