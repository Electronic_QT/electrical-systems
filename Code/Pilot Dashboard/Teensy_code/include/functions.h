/*
    Copyright (C) 2019  Técnico Solar Boat

    This program is free software: you can redistribute 
    it and/or modify it under the terms of the GNU General Public License 
    as published by the Free Software Foundation, either version 3 of the 
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    You can contact Técnico Solar Boat by email at: tecnico.solarboat@gmail.com
    or via our facebook page at https://fb.com/tecnico.solarboat
*/

void myGenieEventHandler();
void onClicked(uint8_t pin, bool heldDown);
void send_throttle();
void send_current_threshold();
void send_solarrelay_button();
void write_tabs();
void write_tab_0();
void write_tab_1();
void write_tab_3();
void write_tab_4();
void write_tab_5();
void write_tab_6();
String float_to_string(float value);
String int_to_string(int value);
void tab_change_plus();
void tab_change_minus();
void write_sniffer();
void energy_calculation();
void reset_kwh();
void tab0_status_decode();
void tab1_status_decode();
void tab1_balancing_decode();
void tab0_warnings_decode();
void tab1_warnings_decode();
void multi1();
void multi2();
void multi3();
void multi4();
void multi5();
void multi6();
void multi7();
void multi8();
void multi9();
void multi10();
void Send_information();
float get_highest_temperature();
