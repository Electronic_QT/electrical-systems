/*
    Copyright (C) 2019  Técnico Solar Boat

    This program is free software: you can redistribute 
    it and/or modify it under the terms of the GNU General Public License 
    as published by the Free Software Foundation, either version 3 of the 
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    You can contact Técnico Solar Boat by email at: tecnico.solarboat@gmail.com
    or via our facebook page at https://fb.com/tecnico.solarboat
*/

#include "TestBench.h"
//#define DEBUGING 1
Adafruit_ADS1115 ads1115_1(ADC_ADDR);
INA_Class INA;
TiPortExpander gpio(GPIO_ADDR);
i2cPot potentiometer(POT_ADDR);
ExponentialFilter<float> FilteredCurrent(40, 0);
ExponentialFilter<float> FilteredTemperature(20, 0);
/***********************************************************************
 * Name:    TestBench()
 * Args:    None
 * Return:  TestBench object
 * Desc:    Constructor for the TestBench Class.Automatically
 *          initializes the pot_level to the theoretical desired
 *          value.
 ***********************************************************************/
TestBench::TestBench()
{
    _level=POT_LEVEL;
        _current=0.0;
        _voltage=0.0;
        _temperature=0.0;
}
/***********************************************************************
 * Name:    begin
 * Args:    None
 * Return:  None
 * Desc:   Start-up sequence for the I2C components. Sets all the
 *         gpio pins as outputs, calibrates the INAS, and set the
 *         potentiometer level to the theoretical desired value 
 ***********************************************************************/
void TestBench::begin(){
    ads1115_1.begin();
    gpio.configurePins(0, 0, 0, 0, 0, 0, 0, 0); //ALL PINS AS OUTPUT
      gpio.writePin(LED1, false);
      gpio.writePin(LED2, false);
      gpio.writePin(LED3, false);
            gpio.writePin(LEDC, false);
      gpio.writePin(RL_BAT, false);
      gpio.writePin(RL_CHG, false);

  INA.begin(INAB_CURR,INAB_R, INA_ADDR);
  //INA.begin(INAC_CURR,INAC_R, INAC_ADDR); INA CHARGE
  INA.setAveraging(128);       // Average each reading n-times     //
  INA.setBusConversion(8500);    // Maximum conversion time 8.244ms  //
  INA.setShuntConversion(8500);  // Maximum conversion time 8.244ms  //
  INA.setMode(INA_MODE_CONTINUOUS_BOTH);
  potentiometer.writeLevel(_level);
}

/***********************************************************************
 * Name:    voltage
 * Args:    None
 * Return:  The cell voltage in volts
 * Desc:   Does a differential read of the ad1115 inputs connected
 *         to the cell and converts it to a real value. Also compensates
 *         for the voltage divider by multiplying by two
 *         
 ***********************************************************************/
float TestBench::voltage() { 
  float readings[10];      // the readings from the analog input
float readIndex = 0;              // the index of the current reading
float total = 0;                  // the running total
float average = 0;                // the average
for (int thisReading = 0; thisReading < 10; thisReading++) 
    readings[thisReading] = 0;
for(int i = 0; i<10;i++){
readings[i] = analogRead(A9)*3.3 /1024.0 * 2;
total = total + readings[i];
}
average = (total+0.5) / 5.0;
return 3.7;
  //return  analogRead(A9)  3.3 /1024.0 * 2;
//ads1115_1.readADC_Differential_0_1() * ADC_16_BIT* 2; 

}
/***********************************************************************
 * Name:    current
 * Args:    The index of the INA to read (0-CHG, 1-DSC)
 * Return:  The chg/dsc current in miliamps
 * Desc:   Reads and convert the value of current measured by the 
 *         INA
 *         
 ***********************************************************************/
float TestBench::current_flowing(int ina_to_read) { 
  float raw_current=-(((INA.getBusMicroAmps(ina_to_read)/1000.0)/1000));
  raw_current=raw_current-5; //Change this after assessing the measured current
                             //deviation
  FilteredCurrent.Filter(raw_current);
  float SmoothCurrent = FilteredCurrent.Current();
  return raw_current;
  }
/***********************************************************************
 * Name:    temperatures
 * Args:    The pin where the thermistor is connected
 * Return:  The thermistor tempeature
 * Desc:   Reads and convert the value of temperature measured by the 
 *         thermistor
 *         
 ***********************************************************************/
float TestBench::temperatures(int THERMISTORPIN) {
  double reading;
  reading = analogRead(THERMISTORPIN);
  float voltage = reading * (VCC / ARD_ADC_BITS);
  float current = voltage / R0;
  float Rthermistor = (VCC - (R0*current)) / current;
  
  // float temperature =
  //     11700.0 / (log(Rthermistor / (R0 * exp(-11700.0 / 298.15)))) - 273.15;
  float temperature = 1.0/(A+B*log(Rthermistor/R25)+C*pow(log(Rthermistor/R25),2) + D*pow(log(Rthermistor/R25),3)) -273.15;
  float RawTemperature = temperature;
  FilteredTemperature.Filter(RawTemperature);
  float SmoothTemperature = FilteredTemperature.Current();
  return SmoothTemperature;
}
/***********************************************************************
 * Name:    discharge
 * Args:    None
 * Return:  The telemetry string
 * Desc:   Controls the disccharge, by monitoring all the parameters
 *         and interveening in case something goes wrong(Eg. the cell
 *         temperature gets too high) or finishing the test when the 
 *         cell is fully discharge. It outputs the telemetry string to be
 *         saved in the SD Card.
 *         
 ***********************************************************************/
String TestBench::discharge(){
    _voltage=voltage();//analogRead(A9)* (VCC / ARD_ADC_BITS)*2;

    _current= current_flowing(INA_SELECT);

    _temperature=temperatures(THERMISTOR_PIN);
          gpio.writePin(RL_BAT, true);

  #ifndef DEBUGING
  if(_voltage > VOLT_THR_LOW && _temperature < TEMP_THR_HIGH){
     gpio.writePin(RL_BAT, true);
  }
  else{
     if(_temperature > TEMP_THR_HIGH)  finished_code(HIGH_TEMP_FINISH);
     //else if (_voltage < 2.95) finished_code(NORMAL_FINISH);
  }
    
  //Feedback for potentiometer
  if(abs(_current)>(1+DESIRED_CUR) || abs(_current)<(DESIRED_CUR-1)){
      if(_current<(DESIRED_CUR-1)) _level+=1;
      else _level-=1;

      potentiometer.writeLevel(_level);
      delay(500);
  }
#endif
  
  String data =  String(_voltage) + ";" + String(_current) + ";" + String(_temperature) +
         ";" + String(millis()) + ";" + String(_level);
  
  return data;
}
 
/***********************************************************************
 * Name:    finished_code
 * Args:    The code of the finished state
 * Return:  None
 * Desc:   Control leds according to the code received. It can either
 *         be a normal finish, where the leds will stay on, or a
 *         or a warning, where all the leds blink. It's a infinite
 *         loop. 
 ***********************************************************************/
void TestBench::finished_code(int num){
  while(1){
  switch(num){
    case HIGH_TEMP_FINISH:
      //gpio.writePin(RL_BAT, false);
      gpio.writePin(LED1, true);
      gpio.writePin(LED2, true);
      gpio.writePin(LED3, true);
      delay(500);
      gpio.writePin(LED1, false);
      gpio.writePin(LED2, false);
      gpio.writePin(LED3, false);
      delay(500);
      break;
    case NORMAL_FINISH:
      //gpio.writePin(RL_BAT, false);
      gpio.writePin(LED1, true);
      gpio.writePin(LED2, true);
      gpio.writePin(LED3, true);
      break;
  }

  }
}